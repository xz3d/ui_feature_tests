#[UI][Finops][Governance] : Budget time limitation field should be in sync with selected time range.
import vulture.ui
#import vulture.api
#from vulture.api import write_to_csv
import time
import os
import sys
import logging

import pytest

def budget_time_limitation() :
    '''
    budget_time_limitation = This function will select the time range for current month and then fetch the time shown on budget time range limitation & validate whether both the time range are in sync.
    '''
    base = vulture.ui.Base("chrome", "false", "windows")
    driver = base.getData()["driver"] 
    driver.implicitly_wait(10)
    driver.maximize_window()

    try:       
        base.login("demo@aquilaclouds.com", "Aquila#Adm1n")
        base.openFinOpsService()

        add_domain = base.getElementByXpath("//span[contains(text(),'Add Domain')]")
        add_domain.click()

        create_budget = base.getElementByXpath('//input[@value="yes"]')
        create_budget.click()
        time.sleep(3) 

        select_time_range = base.getElementById("id_TIME_RANGE")
        select_time_range.click()

        start_month = base.getElementByXpath("//td[@title='2021-09']")
        start_month.click()

        end_month = base.getElementByXpath("//td[@title='2021-09']")
        end_month.click()

        budget_time_limit = base.getElementByXpath("//div[contains(text(),'2021-09-01 to 2021-09-13')]")
        print(budget_time_limit.text)
        
#        assert budget_time_limit.text == "2021-09-01 to 2021-09-30" 
        if budget_time_limit.text == "2021-09-01 to 2021-09-30":
            print("Budget time limitation field is working")
            base.setResult(True)        
        else:
            print("Budget time limitation field is not working")
            base.setResult(False)      
        
    except:
        base.setResult(False)
        driver.quit()

    driver.quit()
    return base.getData()["result"]

def test_542():
    start_time = time.time()
    result = budget_time_limitation()
    end_time = time.time()
    assert result == True
