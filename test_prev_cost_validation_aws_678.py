import vulture.ui
import time
import os
import sys
import logging
from settings import *
from xpaths import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

import pytest

from math import isclose

@pytest.fixture
def check_cost_aws_domain():
    base = vulture.ui.Base(browser, headless, opsys)
    driver = base.getData()["driver"]
    driver.implicitly_wait(10)
    base.login(login, password)
    base.openFinOpsService()
    base.navigateToMenu(2, '#/finops/cldservices')
    tld = base.getElementByXpath(xp_top_level_company_domain_dropdown)
    tld.click()
    aqe = base.getElementByXpath(xp_aq_engineering_menuitem)
    aqe.click()
    apply = base.getElementByXpath(xp_menuitem_top_apply)
    apply.click()
    time.sleep(4)
    provider_type = base.getElementByXpath('//*[@id="popover_providerType"]')
    provider_type.click()
    time.sleep(2)
    aws_option = base.getElementByXpath('//*[contains(text(), "Amazon AWS")]')
    aws_option.click()
    time.sleep(2)
    topover_time_filter = base.getElementByXpath('//*[@title="Last 30 Days"]')
    topover_time_filter.click()
    last_month = base.getElementByXpath('//*[contains(text(), "Last Month")]')
    last_month.click()
    apply_filter = base.getElementByXpath('//button[contains(text(), "Apply")]')
    apply_filter.click()
    time.sleep(2)
    table = base.getElementByXpath('//table[@role="grid"]')
    prev_spend = base.getElementByXpath(xp_prev_bill_spend_cloud_services)
    pspend = prev_spend.text.replace('$', '')
    driver.quit()
    return pspend

@pytest.fixture
def get_val_from_aws_console():
    base = vulture.ui.Base(browser, headless, opsys)
    driver = base.getData()["driver"]
    driver.implicitly_wait(10)
    driver.get("https://244971728197.signin.aws.amazon.com/console")
    time.sleep(5)
    base.login_aws_console("sankrant-eng-user", secret_password)
    time.sleep(5)
    driver.get("https://console.aws.amazon.com/cost-management/home?#/dashboard")
    time.sleep(2)
    explore_costs = base.getElementByXpath('//*[contains(text(), "Explore costs")]/parent::a')
    explore_costs.click()
    time.sleep(2)
    picker_dropdown = base.getElementByXpath('//div[@class="picker-dropdown"]')
    picker_dropdown.click()
    clear_selection = base.getElementByXpath('//*[contains(text(), "Clear Selection")]')
    clear_selection.click()
    input_date_start = base.getElementByXpath('//input[@ng-model="picker.startValue"]')
    input_date_start.send_keys("09/01/2021")
    input_date_end = base.getElementByXpath('//input[@ng-model="picker.endValue"]')
    input_date_end.send_keys("09/30/2021")
    drop_down_apply = base.getElementByXpath('//*[contains(text(), "Apply")]')
    drop_down_apply.click()
    time.sleep(2)
    period_selector = base.getElementByXpath('//button[@title="Daily"]')
    period_selector.click()
    monthly_selector = base.getElementByXpath('//*[contains(text(), "Monthly")]/parent::a')
    monthly_selector.click()
    time.sleep(2)
    cost_td = base.getElementByXpath('//tr[@id="tr-total"]/child::td/following-sibling::td')
    return cost_td.text


def test_cost_validation(check_cost_aws_domain, get_val_from_aws_console):
    print("The value fetched from Aquila is = " + check_cost_aws_domain)
    print("The value fetched from AWS Console is = " + get_val_from_aws_console)
    assert isclose(float(check_cost_aws_domain), float(get_val_from_aws_console), abs_tol=1.9999999)
 