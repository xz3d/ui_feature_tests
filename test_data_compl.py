from pathlib import Path
import pandas as pd
import sqlite3
import glob
import os
from pandas.core.indexes import datetimes
import requests
from vulture.bearer_token import *
from vulture.framework import *

#finops_domain_summary_fetch = "https://qa-finops.aquilaclouds.com/api/finops/domains/summary/fetch"

import pytest

def csv_to_list_for_analysis(filename):
    flag1 = True
    flag2 = True
    csv_header_expsg = ['instance_id', 'group_name', 'currency', 'provider_type', 'time_range_spend', 'current_spend', 'spend_forecast', 'view_chart']
    csv_header_opt_serv_domain = ['gid', 'serv_dom', 'type', 'resource_count', 'created_date', 'updated_date', 'currency', 'current_spend', 'forecast_spend', 'potential_savings', 'opt']
    if filename == 'optservdom.csv':
        data = pd.read_csv(filename, names=csv_header_opt_serv_domain, skiprows=1, keep_default_na=False)
        mainlist = []
        mainlist.append(data.gid.tolist())
        mainlist.append(data.serv_dom.tolist())
        mainlist.append(data.type.tolist())
        mainlist.append(data.resource_count.tolist())
        mainlist.append(data.created_date.tolist())
        mainlist.append(data.updated_date.tolist())
        mainlist.append(data.currency.tolist())
        mainlist.append(data.current_spend.tolist())
        mainlist.append(data.forecast_spend.tolist())
        mainlist.append(data.potential_savings.tolist())
        mainlist.append(data.opt.tolist())
        for i in mainlist:
            s = set(i)
            u = list(s)
            if len(u) > 1:
                if '' in u:
                    flag1 = False
                if 'USD' in u:
                    flag2 = True
    elif filename == 'expsg.csv':
        data = pd.read_csv(filename, names=csv_header_expsg, skiprows=1, keep_default_na=False)
        mainlist = []
        mainlist.append(data.instance_id.tolist())
        mainlist.append(data.group_name.tolist())
        mainlist.append(data.currency.tolist())
        mainlist.append(data.provider_type.tolist())
        mainlist.append(data.time_range_spend.tolist())
        mainlist.append(data.current_spend.tolist())
        mainlist.append(data.spend_forecast.tolist())
        mainlist.append(data.view_chart.tolist())
        for i in mainlist:
            s = set(i)
            u = list(s)
            if len(u) <= 1:
                if '' in u:
                    flag1 = False
                if 'USD' in u:
                    flag2 = True
    else:
        data = pd.read_csv(filename)
    return (flag1, flag2)

def test_dc1():
    none_tests1, _ = csv_to_list_for_analysis("optservdom.csv")
    none_tests2, _ = csv_to_list_for_analysis("expsg.csv")
    if not none_tests2:
        print("Empty fields in explorer -> spend forecast")
    if not none_tests1:
        print("Empty fields in optimizer -> storages") 
    assert none_tests1 and none_tests2

def test_dc2():
    _, currency_tests1 = csv_to_list_for_analysis("optservdom.csv")
    assert currency_tests1