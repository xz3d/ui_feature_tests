# SF-588-[Finops][Governance] : A user with access to a leaf node only, should not be shown the hierarchy that the leaf node belongs to.
import vulture.ui
#import vulture.api
#from vulture.api import write_to_csv
import time
import os
import sys
import logging

import pytest

global flag0
global flag1

flag0 = False
flag1 = False

def leaf_domain() :
    '''
    leaf_domain = This function will fetch the domain link Text and then check the length of domain link. 
    '''
    global flag0
    global flag1

    base = vulture.ui.Base("chrome", "false", "windows")
    driver = base.getData()["driver"]
    driver.implicitly_wait(10)
    driver.maximize_window()

    try:       
        base.login("l@f.com", "Aquila#Adm1n")
        base.openFinOpsService()
        
        domain_list = driver.find_element_by_xpath('//div[@class="breadcrum-container"]')
        print(domain_list.text)
        count = 0
        for i in str.split(domain_list.text, '\n'):
            count += 1      
        print(count)

        if count >= 4: # > 3
            flag0 = False
        else:
            flag0 = True

        validate_domain_link = base.getElementByXpath("//div/span[3][@class='parent-navs clickable']")
        print(validate_domain_link.text)
        
        if validate_domain_link.text == "Leaf node":
            flag1 = True        
        else:
            flag1 = False
        time.sleep(3)      
        
    except:
        base.setResult(False)
        print("Not able to Reproduce")

    if flag0 and flag1 == True:
        base.setResult(True)
    else:
        base.setResult(False)
    driver.quit()
    return base.getData()["result"]

def test_588():
    start_time = time.time()
    result = leaf_domain()
    assert result == True
    end_time = time.time()