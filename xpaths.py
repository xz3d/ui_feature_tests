xp_add_domain_button = '//*[@class="MuiButton-label" and contains(text(), "Add Domain")]/parent::*'
xp_create_budget_pred = '//input[@value="yes"]'
xp_id_groupname = '//*[@id="id_groupName"]'
xp_id_budget = '//*[@id="id_BUDGET"]'
xp_id_time_range = '//*[@id="id_TIME_RANGE"]'

def xp_id_title_date(datestr: str) -> str:
    return "//td[@title=\'"+datestr+"\']"

xp_add_accounts_button = '//*[@class="MuiButton-label" and contains(text(), "Add")]/parent::*'
xp_add_glocal = '//label[@title="Glocal (954881614331)"]/child::input'
xp_fd_apply_button = '//button[contains(text(),"Apply")]'
xp_fd_confirm_apply_button = '//button[contains(text(),"Do you wish to confirm ?")]'
xp_fd_save_button = '//span[contains(text(),"Save")]'
xp_fd_confirm_save_button = '//span[contains(text(),"Confirm")]'
xp_fd_pos_confirm_message_banner = "//div[contains(text(),'Added domain successfully.Cost information will be')]"
xp_sd_pos_confirm_message_banner = "//div[contains(text()),'Added service domain']"
def xp_tripple_dots_domain(domain_name):
    return '//*[@class="dt-cell-wrapper" and @rawdatavalue="'+domain_name+'"]/parent::*/parent::*/parent::*/td[15]/child::div/child::div/child::div/child::button'

def xp_find_domain_name(domain_name):
    return "//div[@value=\""+domain_name+"\"]"

#xp_pagination_input = '//*[@id="pagination-rows"]/following-sibling::input'
xp_pagination_input = '//*[@id="pagination-rows"]'
xp_pagination_val50 = '//li[@data-value="50"]'
xp_delete_button_in_options = '//span[contains(text(), "Delete")]/parent::*/parent::li'
#xp_delete_button_in_options = '//*[contains(text(), "Delete")]/parent::*/following-sibling::span'

xp_i_understand_delete_button = '//span[contains(text(), "I understand, Delete")]/parent::button'
xp_top_level_company_domain_dropdown = '//*[contains(text(), "Top Level Company Domain")]'
xp_aq_engineering_menuitem = '//div[contains(text(), "AQ-Engineering")]'
xp_aq_aws_menuitem = '//div[contains(text(), "AQ_AWS")]'
xp_menuitem_top_apply = '//div[contains(text(), "Apply")]'
xp_prev_bill_spend_cloud_services = '//*[contains(text(), "Previous Bill Spend")]/following-sibling::span/child::span'
xp_curr_bill_spend_cloud_services = '//*[contains(text(), "Current Bill Spend")]/following-sibling::span/child::span'
