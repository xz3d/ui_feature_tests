# SF-598-[Finops][Big Data Services] : Domain Link displays "Container" text instead of "Big data Services".
import vulture.ui
#import vulture.api
#from vulture.api import write_to_csv
import time
import os
import sys
import logging

#from vulture.htmllogger import *

import pytest

def domain_link() :
    '''
    domain_link = This function will fetch the domain link Text and validate it against the actual result. 
    '''
    base = vulture.ui.Base("chrome", "false", "windows")
    driver = base.getData()["driver"]
    driver.implicitly_wait(10)
    driver.maximize_window()

    try:
        #1.Login with the admin user credentials and go to Big Data Services page of explorer.
        #2.Check the text of Domain Link at the top left side of the “Big data Services” Page
        
        base.login("demo@aquilaclouds.com", "Aquila#Adm1n")
        base.openFinOpsService()
        
        base.navigateToMenu(2, '#/finops/bigdataservices')        

        validate_domain_link = base.getElementByXpath('//div/span[2][@class="parent-navs"]')
        print(validate_domain_link.text)
        
        if validate_domain_link.text == "Big Data Services":
            print("Domain Link displays Correct Details")
            base.setResult(True)        
        else:
            print("Domain Link Displays incorrect Details")
            base.setResult(False)
        time.sleep(5)       

    except:
        base.setResult(False)
        print("Not able to Reproduce")

    driver.quit()
    return base.getData()["result"]

"""
@pytest.mark.skip(reason="skipping exec")
def exec_598():
    bugId = os.path.basename(__file__).split('.', 1)[0]
    print("Running " + bugId)
    start_time = time.time()
    setup("ISSUE 598 - Verify BDS Correct Label", '''Domain Link displays "Container" text instead of "Big data Services"''', mode='a')
    result = domain_link()
    dbg("TEST TYPE: UI")
    if result == True:
        print("ISSUE 598 validate_domain_link_598 TEST: PASSED -> Domain Link displays Correct Details")
        info("Test Passed: Domain Link Displays Correct Details")
    else:
        print("ISSUE 598 validate_domain_link_598 TEST: FAILED -> Domain Link Displays incorrect Details")
        warn("ERROR: Domain Link Displays incorrect details")
        err("TEST FAILED")
"""

def test_598():
    start_time = time.time()
    result = domain_link()
    assert result == True
