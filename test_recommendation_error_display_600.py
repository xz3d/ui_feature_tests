# ISSUE 600
# Method: UI
import vulture.ui
import time

import pytest


def recommendation_error_dispaly():
    '''
    As per the reported issue, checks for the display of error called
    "Data Not Available", while logged in as o@v.com at #/optimizer/storages
    '''
    base = vulture.ui.Base("chrome", "false", "windows")
    driver = base.getData()["driver"]
    driver.implicitly_wait(10)
    try:
        base.login("o@v.com", "Aquila#Adm1n")
        base.openFinOpsService()
        base.navigateToMenu(4, '#/optimizer/storages')
        time.sleep(10)
        try:
            dna = base.getElementByXpath('//*[@id="rc-tabs-0-panel-3"]/div[2]/span')
            if dna.text == "Data Not Available":
                base.setResult(False)
        except:
            base.setResult(True)
    except:
        # handle errors
        base.setResult(False)
        print("Test Failed")
    driver.quit()
    return base.getData()["result"]


def test_600():
    result = recommendation_error_dispaly()
    assert result == True