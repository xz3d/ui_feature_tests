# ISSUE 601
# Method: API

'''
This test case calls the servdomain/save?type=add api to add a service domain
It dispays the status flag and optionally error messages if the test fails.
'''

from logging import debug
import json
from vulture.bearer_token import *
from vulture.framework import *

import pytest

def add_service_domain():
    url = "https://qa-finops.aquilaclouds.com/api/finops/servdomain/save?type=add"
    payload="{\"userId\":16023948,\"envList\":[391973,389588,385172,371777,133780,113880,388572,353193,297289,2107311,5299327],\"serviceKey\":\"finops\",\"params\":[{\"key\":\"formNewDomain\",\"formData\":{\"groupId\":\"\",\"groupName\":\"zed\"}},{\"key\":\"resourcesSelection\",\"values\":[]}]}"
    headers = get_headers("a@b.com", "Aquila#Adm1n")
    response = requests.request("POST", url, headers=headers, data=payload)
    responsejson = json.loads(response.text)
    return (response, responsejson)

def exec_601():
    status, data = add_service_domain()
    if str(status) == '<Response [200]>':
        if (data['key'] == "ADD_EDIT_DOMAIN_SUCCESS"):
            print("TEST add_service_domain_601: PASS")
            print("KEY: " + str(data['key']))
            print("STATUS: " + str(data['status']))
            print("MESSAGE: " +str(data['message']))
            print("VARIANT: " + str(data['variant']))
        else:
            print("TEST add_service_domain_601 (ISSUE 601): FAIL")
            print("KEY: " + str(data['key']))
            print("STATUS: " + str(data['status']))
            print("MESSAGE: " + str(data['message']))
            print("VARIANT: " + str(data['variant']))

def test_601():
    status, data = add_service_domain()
    assert str(status) == '<Response [200]>' and data['key'] == "ADD_EDIT_DOMAIN_SUCCESS" 
    assert data['key'] == "ADD_EDIT_DOMAIN_SUCCESS"
