import vulture.ui
import time
import os
import sys
import logging
from settings import *
from xpaths import *

def add_new_serv_domain(name: str) :
    base = vulture.ui.Base(browser, headless, opsys)
    driver = base.getData()["driver"]
    driver.implicitly_wait(10)
    base.login(login, password)
    base.openFinOpsService()
    time.sleep(5)
    base.navigateToMenu(3, '#/governance/servdomain')
    add_domain = base.getElementByXpath(xp_add_domain_button)
    add_domain.click()
    time.sleep(5)
    enter_domain_name = base.getElementByXpath(xp_id_groupname)
    enter_domain_name.send_keys(name)
    time.sleep(5)
    save_button = base.getElementByXpath(xp_fd_save_button)
    save_button.click()
    confirm_save = base.getElementByXpath(xp_fd_confirm_save_button) 
    confirm_save.click()
    time.sleep(2)
    validate = base.getElementByXpath(xp_find_domain_name(name))
    driver.quit()
    return validate != 0


def validate():
    testCaseName = os.path.basename(__file__).split('.', 1)[0]
    print("Running " + testCaseName)
    start_time = time.time()
    result = add_new_serv_domain("zedtest1_serv_domain")
    if result == True:
        print("Add Service Domain TEST: PASSED -> Scenario PASS")
    else:
        print("Add Service Domain TEST: FAILED -> Scenario FAIL")
    print("Finished in: %s seconds" % (time.time() - start_time))
    return result

def test_serv_domain():
    result = validate
    assert result