import vulture.ui
import time
import os
import sys
import logging
from settings import *
from xpaths import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

import pytest

def paginate_max(base, driver):
    pagination = base.getElementByXpath(xp_pagination_input)
    pagination.click()
    time.sleep(1)
    pagination_val = base.getElementByXpath(xp_pagination_val50)
    pagination_val.click()
    time.sleep(1)

# Add Domain
def add_domain(name: str, budget: int, base, driver):
    add_domain = base.getElementByXpath(xp_add_domain_button)
    add_domain.click()
    time.sleep(5)
    enter_domain_name = base.getElementByXpath(xp_id_groupname)
    enter_domain_name.send_keys(name)
    time.sleep(5)
    create_budget = base.getElementByXpath(xp_create_budget_pred)
    create_budget.click()
    budget_input = base.getElementByXpath(xp_id_budget)
    budget_input.send_keys(budget)
    #reset_period = base.selectDropdownOption(0, 'Monthly')
    select_time_range = base.getElementByXpath(xp_id_time_range)
    select_time_range.click()
    start_month = base.getElementByXpath(xp_id_title_date('2021-09'))
    start_month.click()
    end_month = base.getElementByXpath(xp_id_title_date('2021-09'))
    end_month.click()
    add_accounts = base.getElementByXpath(xp_add_accounts_button)
    add_accounts.click()
    time.sleep(5)
    add_glocal = base.getElementByXpath(xp_add_glocal)
    add_glocal.click()
    time.sleep(5)
    apply_button = base.getElementByXpath(xp_fd_apply_button)
    apply_button.click()
    confirm_apply = base.getElementByXpath(xp_fd_confirm_apply_button)
    confirm_apply.click()
    time.sleep(2)
    save_button = base.getElementByXpath(xp_fd_save_button)
    save_button.click()
    confirm_save = base.getElementByXpath(xp_fd_confirm_save_button) 
    confirm_save.click()
    c_message = base.getElementByXpath(xp_fd_pos_confirm_message_banner)
    return c_message

def options_edit(name, base, driver):
    go_to_options = base.getElementByXpath(xp_tripple_dots_domain(name))
    go_to_options.click()
    time.sleep(2)
    action = ActionChains(driver)
    edit = driver.switch_to.active_element
    edit.send_keys(Keys.ENTER)

def edit_fin_domain(name: str, new_name: str, budget: int, base, driver) :
    base.login(login, password)
    base.openFinOpsService()
    time.sleep(5)
#    try:
#        add_domain(name, 1000, base, driver)
#    except:
#        print("Cannot add domain")
    time.sleep(2)
    driver.refresh()
    time.sleep(5)
    try:
        paginate_max(base, driver)
        time.sleep(3)
        options_edit(name, base, driver)
    except:
        driver.get("https://qa-finops.aquilaclouds.com/#/governance/edit-finance-domain?groupId=580")
    time.sleep(5)
    enter_domain_name = base.getElementByXpath(xp_id_groupname)
    enter_domain_name.send_keys(Keys.CONTROL + "a")
    enter_domain_name.send_keys(Keys.DELETE)
    enter_domain_name.send_keys(new_name)
    time.sleep(5)
    create_budget = base.getElementByXpath(xp_create_budget_pred)
    create_budget.click()
    budget_input = base.getElementByXpath(xp_id_budget)
    budget_input.send_keys(Keys.CONTROL + "a")
    budget_input.send_keys(Keys.DELETE)
    budget_input.send_keys(budget)
    #reset_period = base.selectDropdownOption(0, 'Monthly')
    select_time_range = base.getElementByXpath(xp_id_time_range)
    select_time_range.click()
    start_month = base.getElementByXpath(xp_id_title_date('2021-09'))
    start_month.click()
    end_month = base.getElementByXpath(xp_id_title_date('2021-09'))
    end_month.click()
    save_button = base.getElementByXpath(xp_fd_save_button)
    save_button.click()
    confirm_save = base.getElementByXpath(xp_fd_confirm_save_button) 
    confirm_save.click()
    time.sleep(2)
    paginate_max(base, driver)
    validate = base.getElementByXpath(xp_find_domain_name(name))
    return validate != 0


def test_edit_fin_domain():
    base = vulture.ui.Base(browser, headless, opsys)
    driver = base.getData()["driver"]
    driver.implicitly_wait(10)
    testCaseName = os.path.basename(__file__).split('.', 1)[0]
    print("Running " + testCaseName)
    start_time = time.time()
    result = edit_fin_domain("ced3","led3", 2000, base, driver)
    assert result
    print("Finished in: %s seconds" % (time.time() - start_time))