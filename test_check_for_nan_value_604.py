# ISSUE 604
# Method: UI
from test_add_service_domain_601 import exec_601
import vulture.ui
import time

import pytest

def test_check_for_nan_values():
    '''
    While logged in as o@v.com and navigating to #/governance/overview
    this test case checks for any appearance of NaN values appearing on the page
    and marks the test as fail if it does.
    '''

    # TODO(Sankrant): Config File for Hardcoded values
    base = vulture.ui.Base("chrome", "false", "windows")
    driver = base.getData()["driver"]
    driver.implicitly_wait(10)
    try:
        base.login("o@v.com", "Aquila#Adm1n")
        base.openFinOpsService()
        base.navigateToMenu(3, '#/governance/overview')
        time.sleep(20)
        try:
            dna0 = base.getElementByXpath('//*[@class="prog-val" and contains(text(), "NaN")]')
            print(dna0.text)
            x = dna0.get_attribute("innerText")
            print(x)
            if "NaN" in dna0.text:
                print("Error Caught: NaN")
                base.setResult(False)
        except:
            base.setResult(True)
    except:
        # handle errors
        base.setResult(False)
        print("Test Failed")
    driver.quit()
    return base.getData()["result"]

def test_604():    
    result = test_check_for_nan_values()
    assert result == True