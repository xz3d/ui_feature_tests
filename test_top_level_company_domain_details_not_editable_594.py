# SF-594- [Finops][Governance]: Top Level Company Domain Details is not Editable.
import vulture.ui
import time
import os

import pytest


def edit_toplevel_domain(name: str) :
    '''
    edit_toplevel_domain = This function will check if the top level company domain is present or not on details table and if not, it should return "No domains available" message.  
    '''
    base = vulture.ui.Base("chrome", "false", "windows")
    driver = base.getData()["driver"]
    driver.implicitly_wait(10)
    driver.maximize_window()

    try:
        # 1: login with the admin role user, which has access to top level company Domain and it should display top level company domain on the details table.
        # 2: If Top level Company domain is not available on details table, then issue has been reproduced.
        base.login("demo@aquilaclouds.com", "Aquila#Adm1n")
        base.openFinOpsService()

        enter_search_term = base.getElementByXpath('//input[@type="text"]')
        enter_search_term.send_keys(name)

        find_search_item = base.getElementByXpath('//span[@class="ant-input-group-addon"]')
        find_search_item.click()

        validate_domain = base.getElementByXpath("//div[contains(text(),'No Domains available')]")
        print(validate_domain.text)

        if validate_domain.text == "No Domains available":
            print("Not able to edit company domain, as company domain not found")
            base.setResult(False)
        else:
            print("Top level Company Domain Found")
            base.setResult(True)

    except:
        base.setResult(False)
        print("Not able to reproduce")

    driver.quit()
    return base.getData()["result"]

def test_594():
    start_time = time.time()
    result = edit_toplevel_domain("Top Level Company Domain")
    assert result == True
