import vulture.ui
import time
import os
import sys
import logging
from settings import *
from xpaths import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains


def add_domain(name: str, budget: int, base, driver):
    add_domain = base.getElementByXpath(xp_add_domain_button)
    add_domain.click()
    time.sleep(5)
    enter_domain_name = base.getElementByXpath(xp_id_groupname)
    enter_domain_name.send_keys(name)
    time.sleep(5)
    create_budget = base.getElementByXpath(xp_create_budget_pred)
    create_budget.click()
    budget_input = base.getElementByXpath(xp_id_budget)
    budget_input.send_keys(budget)
    #reset_period = base.selectDropdownOption(0, 'Monthly')
    select_time_range = base.getElementByXpath(xp_id_time_range)
    select_time_range.click()
    start_month = base.getElementByXpath(xp_id_title_date('2021-09'))
    start_month.click()
    end_month = base.getElementByXpath(xp_id_title_date('2021-09'))
    end_month.click()
    add_accounts = base.getElementByXpath(xp_add_accounts_button)
    add_accounts.click()
    time.sleep(5)
    add_glocal = base.getElementByXpath(xp_add_glocal)
    add_glocal.click()
    time.sleep(5)
    apply_button = base.getElementByXpath(xp_fd_apply_button)
    apply_button.click()
    confirm_apply = base.getElementByXpath(xp_fd_confirm_apply_button)
    confirm_apply.click()
    time.sleep(2)
    save_button = base.getElementByXpath(xp_fd_save_button)
    save_button.click()
    confirm_save = base.getElementByXpath(xp_fd_confirm_save_button) 
    confirm_save.click()
    c_message = base.getElementByXpath(xp_fd_pos_confirm_message_banner)
    return c_message.text

def paginate_max(base, driver):
    pagination = base.getElementByXpath(xp_pagination_input)
    pagination.click()
    time.sleep(1)
    pagination_val = base.getElementByXpath(xp_pagination_val50)
    pagination_val.click()
    time.sleep(1)

def options_delete(name, base, driver):
    go_to_options = base.getElementByXpath(xp_tripple_dots_domain(name))
    go_to_options.click()
    time.sleep(2)
    edit = driver.switch_to.active_element
    edit.send_keys(Keys.ARROW_DOWN)
    delete = driver.switch_to.active_element
    delete.send_keys(Keys.ENTER)
    time.sleep(6)
    delete_button = base.getElementByXpath('//span[contains(text(), "I understand, Delete")]/parent::button')
    delete_button.click()


def add_new_fin_domain(name: str, budget: int, base, driver) :
    base.login(login, password)
    time.sleep(5)
    base.openFinOpsService()
    time.sleep(5)
    message = add_domain(name, budget, base, driver)
    if message == 'Added domain successfully.Cost information will be available in 15 minutes.':
        base.setResult(True)
    paginate_max(base, driver)
    options_delete(name, base, driver)
    time.sleep(10)
    ## Validation

    #1. Check if the confirmation message was positive
    #2. Check if the domain name appears in the list
    #3. Check if the budget amount is set correctly
    #4. Check if the dates are set correctly
    
   # except:
    #    base.setResult(False)
   #     print("Test Failed")
   
    driver.quit()
    return base.getData()["result"]

def test_add_fin_domain():
    base = vulture.ui.Base(browser, headless, opsys)
    driver = base.getData()["driver"]
    driver.implicitly_wait(10)
    result = add_new_fin_domain("zed5", 2004, base, driver)
    assert result