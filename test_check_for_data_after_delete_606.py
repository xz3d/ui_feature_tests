# ISSUE 606
# Method: API

'''
This test case checks if the domain data is still visible for the user after deleting it
'''

import json

from vulture.bearer_token import *
from vulture.framework import *
#from vulture.htmllogger import *

import pytest

def delete_domain_payload(gid):
    return "{\"userId\":368543,\"envList\":[391973,389588,385172,371777,133780,113880,388572,353193,297289,2107311,5299327],\"serviceKey\":\"finops\",\"params\":[{\"key\":\"groupId\",\"value\":"+str(gid)+"}]}"

def add_user_with_gid(num): 
    return "{\"userId\":16023948,\"envList\":[391973,389588,385172,371777,133780,113880,388572,353193,297289,2107311,5299327],\"serviceKey\":\"finops\",\"params\":[{\"key\":\"status\",\"value\":true},{\"key\":\"firstName\",\"value\":\"test606\"},{\"key\":\"lastName\",\"value\":\"zed\"},{\"key\":\"role\",\"list\":[\"FinOps Admin\"]},{\"key\":\"userId\",\"value\":\"\"},{\"key\":\"email\",\"value\":\"t@z.com\"},{\"key\":\"type\",\"list\":[\"LOCAL\"]},{\"key\":\"loginConfigParam\",\"list\":[]},{\"key\":\"password\",\"value\":\"Aquila#Adm1n\"},{\"key\":\"confpassword\",\"value\":\"Aquila#Adm1n\"},{\"key\":\"preferences\",\"list\":[]},{\"key\":\"domainAdminId\",\"list\":[\"" + str(num) + "\"]},{\"key\":\"domainEditorId\",\"list\":[]},{\"key\":\"domainViewerId\",\"list\":[]},{\"value\":\"\"},{\"key\":\"awsAccounts\",\"list\":[]},{\"key\":\"awsCustomers\",\"list\":[]},{\"value\":\"\"},{\"key\":\"azureAccounts\",\"list\":[]},{\"key\":\"azureCspCustomers\",\"list\":[]},{\"key\":\"azureStackAccounts\",\"list\":[]},{\"key\":\"azureStackSubscriptions\",\"list\":[]},{\"value\":\"\"},{\"key\":\"ociSubscriptions\",\"list\":[]},{\"key\":\"ociCustomers\",\"list\":[]},{\"value\":\"\"},{\"key\":\"gcpProjects\",\"list\":[]},{\"key\":\"gcpCustomers\",\"list\":[]}],\"drillParams\":{}}"

def delete_tz_user():
    delete_user = "https://qa-finops.aquilaclouds.com/api/user/deleteuser"
    delete_user_payload = "{\"userId\":368543,\"envList\":[391973,389588,385172,371777,133780,113880,388572,353193,297289,2107311,5299327],\"serviceKey\":\"finops\",\"params\":[{\"key\":\"userId\",\"value\":16092885},{\"key\":\"email\",\"value\":\"t@z.com\"}]}"
    header_delete_user = get_headers(user="demo@aquilaclouds.com", password="Aquila#Adm1n")
    response = requests.request("POST", delete_user, headers=header_delete_user, data=delete_user_payload)
    responsejson = json.loads(response.text)
    return response, responsejson

'''
domain_fetch = "https://qa-finops.aquilaclouds.com/api/finops/domains/summary/fetch"
domain_fetch_payload = "{\"drillParams\":{\"groupId\":\"96\"},\"userId\":368543,\"envList\":[391973,389588,385172,371777,133780,113880,388572,353193,297289,2107311,5299327],\"serviceKey\":\"finops\"}"
header_domain_fetch = get_headers(user="demo@aquilaclouds.com", password="Aquila#Adm1n")
response = requests.request("POST", domain_fetch, headers=header_domain_fetch, data=domain_fetch_payload)
responsejson = json.loads(response.text)

header_delete = get_headers(user="demo@aquilaclouds.com", password="Aquila#Adm1n")
for i in responsejson:
    if i['groupName'] == 'test606':
        delete_domain = "https://qa-finops.aquilaclouds.com/api/finops/domain/delete"
        response = requests.request("POST", delete_domain, headers=header_delete, data=delete_domain_payload1(i['groupId']))
        responsejson = json.loads(response.text)
        break
'''

def add_tz_domain():
    add_domain = "https://qa-finops.aquilaclouds.com/api/finops/domain/save?type=add"
    add_domain_payload = "{\"userId\":368543,\"envList\":[391973,389588,385172,371777,133780,113880,388572,353193,297289,2107311,5299327],\"serviceKey\":\"finops\",\"params\":[{\"key\":\"formNewDomain\",\"formData\":{\"groupId\":\"96\",\"groupName\":\"test606\",\"CREATE_BUDGET\":\"yes\",\"BUDGET\":\"1000\",\"RESET_PERIOD\":\"MONTHLY\",\"CURRENCY\":\"\",\"TIME_RANGE\":\"1631528415634,1634120415634\"}},{\"key\":\"accountsSelection\",\"values\":[{\"label\":\"Aquila_OCI\",\"key\":\"389588\",\"type\":\"cluster\",\"row\":null,\"children\":[{\"label\":\"7901610 (7901610)\",\"key\":\"7901610\",\"type\":\"service\",\"row\":null,\"children\":[]}]}]},{\"key\":\"tagsSelection\"},{\"key\":\"cldsevicesSelection\"},{\"key\":\"containersSelection\"},{\"key\":\"resourcesSelection\"},{\"key\":\"resGrpSelection\"}]}"
    header_add_domain = get_headers(user="demo@aquilaclouds.com", password="Aquila#Adm1n")
    response = requests.request("POST", add_domain, headers=header_add_domain, data=add_domain_payload)
    responsejson = json.loads(response.text)
    assert(str(response) == '<Response [200]>')
    domain_fetch = "https://qa-finops.aquilaclouds.com/api/finops/domains/summary/fetch"
    domain_fetch_payload = "{\"drillParams\":{\"groupId\":\"96\"},\"userId\":368543,\"envList\":[391973,389588,385172,371777,133780,113880,388572,353193,297289,2107311,5299327],\"serviceKey\":\"finops\"}"
    header_domain_fetch = get_headers(user="demo@aquilaclouds.com", password="Aquila#Adm1n")
    response = requests.request("POST", domain_fetch, headers=header_domain_fetch, data=domain_fetch_payload)
    responsejson = json.loads(response.text)
    gnum = 0
    for i in responsejson:
        if i['groupName'] == 'test606':
            gnum = i['groupId']
            break
    return response, responsejson, gnum

def add_user(gid):
    add_user = "https://qa-finops.aquilaclouds.com/api/user/addedituser"
    header_add_user = get_headers(user="a@b.com", password="Aquila#Adm1n")
    response = requests.request("POST", add_user, headers=header_add_user, data=add_user_with_gid(gid))
    responsejson = json.loads(response.text)
    return response, responsejson

def delete_domain(gid):
    delete_domain = "https://qa-finops.aquilaclouds.com/api/finops/domain/delete"
    header_delete = get_headers(user="demo@aquilaclouds.com", password="Aquila#Adm1n")
    response = requests.request("POST", delete_domain, headers=header_delete, data=delete_domain_payload(gid))
    responsejson = json.loads(response.text)
    return response, responsejson

def check_opt_storage_from_created_user():
    opt_fetch_storage = "https://qa-finops.aquilaclouds.com/api/finops/optimizer/storage/cost/fetch"
    opt_storage_payload="{\"drillParams\":{\"groupId\":\"96\",\"providerType\":\"\"},\"userId\":16092951,\"envList\":[391973,389588,385172,371777,133780,113880,388572,353193,297289,2107311,5299327],\"serviceKey\":\"finops\"}" 
    opt_header = get_headers(user="t@z.com", password="Aquila#Adm1n")
    response = requests.request("POST", opt_fetch_storage, headers=opt_header, data=opt_storage_payload)
    responsejson = json.loads(response.text)
    if (len(responsejson) > 0):
        return False
    else:
        return True


def test_606():
    delete_tz_user()
    staus, _, gid = add_tz_domain()
    staus, _ = add_user(gid)
    staus, _ = delete_domain(gid)
    flag = check_opt_storage_from_created_user()
    assert flag == True
