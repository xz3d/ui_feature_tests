import sys
import os
import re
import time
import datetime
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options as CO
from selenium.webdriver.firefox.options import Options as FO
from selenium.webdriver import ActionChains

class Base:
    def __init__(self, driverName, headless, osName):
        # driverName: "firefox" or "chrome"
        # headless: True or False, DO NOT USE CHROME IN HEADLESS
        # osName: depreciated, can pass in anything
        self.data = {}

        if(headless.lower() == "false"):
            self.data["headless"] = False
        else:
            self.data["headless"] = True

        self.browser(driverName, osName)
        self.data["url"] = ''
        self.data["result"] = True

    def browser(self, driverName, osName):
        # driverName: "firefox" or "chrome"
        # osName: depreciated, can pass in anything
        # Defines which browser to use to run UI test cases
        
        dirname = os.path.dirname(__file__)
        directory = os.path.join(dirname, '')
        

        # If using chrome webdriver
        if(driverName == "chrome"):
            options = CO()
            options.headless = self.data["headless"]
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.add_experimental_option("prefs", {
                "download.default_directory": directory,
                "download.prompt_for_download": False,
                "download.directory_upgrade": True,
                "safebrowsing.enabled": True
            })
            options.add_experimental_option("excludeSwitches", ["enable-logging"])
            self.data["driver"] = webdriver.Chrome(
                chrome_options=options)
            self.data["driver"].implicitly_wait(30)

        # If using firefox webdriver
        elif(driverName == "firefox"):
            options = FO()
            options.headless = self.data["headless"]
            profile = webdriver.FirefoxProfile()
            profile.set_preference("browser.download.folderList", 2)
            profile.set_preference(
                "browser.download.manager.showWhenStarting", False)
            profile.set_preference("browser.download.dir", directory)
            profile.set_preference(
                "browser.helperApps.neverAsk.saveToDisk", "text/csv;image/png;image/jpeg;image/svg+xml")
            self.data["driver"] = webdriver.Firefox(
                firefox_profile=profile, options=options)
            self.data["driver"].implicitly_wait(30)

    def login(self, username, password):
        # username: Username of the user for Hawk
        # password: Password of the user for Hawk
        # Logs in wih the given credentials

        self.data["url"] = "https://qa-finops.aquilaclouds.com/"
        self.data["driver"].get(self.data["url"])

        try:
            usernameElement = self.data["driver"].find_element_by_id("email")
            usernameElement.clear()
            usernameElement.send_keys(username)

        except:
            print("Cannot find element called email")
            self.data["result"] = False


        self.data["driver"].find_element(
            By.XPATH, '//button[@type="button"]/span[text()="NEXT"]').click()

        try:
            passwordElement = self.data["driver"].find_element_by_id("pass")
            passwordElement.clear()
            passwordElement.send_keys(password)
        except:
            print("Cannot find element called pass")
            self.data["result"] = False

        self.data["driver"].find_element(
            By.XPATH, '//button[@type="button"]/span[text()="SIGN IN"]').click()


    def login_aws_console(self, username, password):
        # username: Username of the user for Hawk
        # password: Password of the user for Hawk
        # Logs in wih the given credentials
        try:
            usernameElement = self.data["driver"].find_element_by_id("username")
            usernameElement.clear()
            usernameElement.send_keys(username)

        except:
            print("Cannot find element called email")
            self.data["result"] = False
        try:
            passwordElement = self.data["driver"].find_element_by_id("password")
            passwordElement.clear()
            passwordElement.send_keys(password)
        except:
            print("Cannot find element called pass")
            self.data["result"] = False

        apply_btn = self.data["driver"].find_element_by_id("signin_button")
        apply_btn.click()

    def loginOKTA(self, username, password):
        # username: Username of the user for Hawk
        # password: Password of the user for Hawk
        # Logs in wih the given credentials using OKTA

        self.data["driver"].get("http://52.152.227.83:8080/")

        try:
            usernameElement = self.data["driver"].find_element_by_id("email")
            usernameElement.clear()
            usernameElement.send_keys(username)
        except:
            print("Cannot find element called email")

        self.data["driver"].find_element(By.XPATH, '//button[@type="button"]/span[text()="NEXT"]').click()

        try:
            passwordElement = self.data["driver"].find_element(By.XPATH, '//*[@id="okta-signin-password"]')
            passwordElement.clear()
            passwordElement.send_keys(password)
        except:
            print("Cannot find element called password")

        self.data["driver"].find_element(By.XPATH, '//*[@id="okta-signin-submit"]').click()

    def testLogin(self, username, password, shouldFail):
        # username: Username of the user for Hawk
        # password: Password of the user for Hawk
        # shouldFail: True or False. If the login should fail or not. If fails, result is True else False
        # Logs in wih the given credentials

        self.data["url"] = "http://52.152.227.83:8080/"
        self.data["driver"].get(self.data["url"])

        try:
            usernameElement = self.data["driver"].find_element_by_id("email")
            usernameElement.clear()
            usernameElement.send_keys(username)

        except:
            print("Cannot find element called email")
            self.data["result"] = shouldFail


        self.data["driver"].find_element(
            By.XPATH, '//button[@type="button"]/span[text()="NEXT"]').click()

        try:
            passwordElement = self.data["driver"].find_element_by_id("pass")
            passwordElement.clear()
            passwordElement.send_keys(password)
        except:
            print("Cannot find element called pass")
            self.data["result"] = shouldFail

        self.data["driver"].find_element(
            By.XPATH, '//button[@type="button"]/span[text()="SIGN IN"]').click()

    def getElementByXpath(self, xpath, wait = 30, multipleElements = False):
        # xpath: XPATH of the element trying to find
        # wait: How long should the driver wait before giving an error
        # multipleElements: True or False, default is False. If there are multiple elements with the same XPATH.
        # Returns: A single element or a list of elements depending on 'multipleElements'. -1 if not found

        self.data["driver"].implicitly_wait(wait)

        if (multipleElements):
            try:
                return self.data["driver"].find_elements(By.XPATH, xpath)
            except:
                return -1

        else:
            try:
                return self.data["driver"].find_element(By.XPATH, xpath)
            except:
                return -1

    def findSubElement(self, parentElement, xpath, multipleElements = False):
        # parentElement: The parent element object (returned by getElemenByXpath())
        # xpath: XPATH of the element trying to find
        # multipleElements: True or False, default is False. If there are multiple elements with the same XPATH.
        # Returns: A single element or a list of elements depending on 'multipleElements'. -1 if not found

        if (multipleElements):
            try:
                return parentElement.find_elements(By.XPATH, xpath)
            except:
                return -1

        else:
            try:
                return parentElement.find_element(By.XPATH, xpath)
            except:
                return -1

    def getData(self):
        # Returns: Data stored in the current Base class object

        return self.data

    def setResult(self, value):
        # Value: True or False
        # Set test case result to value

        self.data["result"] = value

    def clickButton(self, buttonType, buttonText, index = 0):
        # buttonType: Type of the button. Can be found in the button's XPATH
        # buttonText: Text attribute of the button.
        # index: The button number, helpful in case there are multiple buttons with the same buttonType and buttonText
        # Returns: Clicks the button if found, else prints out the 'buttonText button not found'

        time.sleep(3)
        try:

            try:
                self.data["driver"].find_elements(By.XPATH, f'//button[@type="{buttonType}"]/span[1][text()="{buttonText}"]')[index].click()
            except:
                self.data["driver"].find_elements(By.XPATH, f'//button[@type="{buttonType}"][text()="{buttonText}"]')[index].click()

        except:
            print(f"{buttonText} button not found")

    def getInputFields(self, inputType):
        # inputType: Type of the input. Can be found in the input's XPATH
        # Returns: A list of input elements of the type 'inputType'

        return self.data["driver"].find_elements(By.XPATH, f'//input[@type="{inputType}"]')

    def selectDropdownOption(self, index, optionName):
        # index: Dropdown menu number on the screen. Starts from 0
        # optionName: Name of the option inside the dropdown menu
        # Returns: Selects the given dropdown option from the dropdown menu

        time.sleep(2)
        selector = self.data["driver"].find_elements(By.XPATH, '//div[@class="ant-select-selector"]')
        
        # print(len(selector))

        if(len(selector) == 0 or index >= len(selector)):
            print("Dropdown menu not found")
            self.data["result"] = False
            return False
        else:
            selector[index].click()

        try:

            try:
                self.data["driver"].find_element(By.XPATH, f'//div[@class="ant-select-item-option-content"][text()="{optionName}"]').click()
            except:
                dropDownInput = selector[index].find_element(By.XPATH, './/span/input')
                dropDownInput.clear()
                dropDownInput.send_keys(optionName)
                dropDownInput.send_keys(Keys.RETURN)

        except:
            self.data["result"] = False
            print(f"{optionName} option not found")

    def sendInput(self, inputFieldsList, inputText, inputIndex, placeholder = "default"):
        # inputFieldsList: List of inputs of the same type returned by 'getInputFields()'. Pass -1 in case no of no fields
        # inputText: Text that you want to pass in as input
        # inputIndex: Index of the input element on screen. Starts from 0
        # placeholder: The placeholder inside the input element. Can be found in element's placeholder attribute,
        #              no need to pass this if inputFieldsList is not -1
        # Returns: Sends the input text given to the given input element.

        try:
            if(inputFieldsList != -1):
                try:
                    inputFieldsList[inputIndex].clear()
                except:
                    print("")

                inputFieldsList[inputIndex].send_keys(inputText)
                inputFieldsList[inputIndex].send_keys(Keys.RETURN)

            elif(inputFieldsList == -1):
                inputField = self.data["driver"].find_elements(By.XPATH, f'//input[@placeholder="{placeholder}"]')[inputIndex]
                try:
                    inputField.clear()
                except:
                    print("")

                inputField.send_keys(inputText)
                inputField.send_keys(Keys.RETURN)

        except:
            self.data["result"] = False
            print(f"Could not find input field to input {inputText}")

    def showColumnInTable(self, columnIndex, index = 0):
        # columnIndex: Index of the option inside 'View Columns' menu. Can be found from the option's XPATH. Starts from 1 usually
        # index: Index of the 'View Columns' button. Usually the first button is 0. Starts from 0
        # Returns: Name of the column

        self.data["driver"].find_elements(By.XPATH, '//button[@aria-label="View Columns"]')[index].click()
        self.data["driver"].find_element(By.XPATH, f'//fieldset/div/label[{columnIndex}]/span[1]/span[1]/input').click()
        self.data["driver"].find_element(By.XPATH, '//button[@aria-label="Close"]').click()
        
        return self.data["driver"].find_element(By.XPATH, f'//fieldset/div/label[{columnIndex}]/span[2]').text

    def clickTableHeaderButton(self, buttonName, index = 0):
        # buttonName: Name of the header in the table
        # index: Index of the button. Can be found out by looking at the order on screen. Starts from 0.
        #        Leave at 0 if there is only 1 column header of the given name on the ENTIRE page.
        # Clicks the given button in the table header.

        self.data["driver"].find_elements(By.XPATH, f'//button[@aria-label="{buttonName}"]')[index].click()
    
    def openFinOpsService(self, entrare="open-btn", vararg=0):
        # Opens the fin ops service page
        self.data["driver"].implicitly_wait(5)
        self.data["driver"].find_elements(
            By.XPATH, '//div[@class=\"'+str(entrare)+'\"][text()="Open "]')[vararg].click()

    def changePagesInTable(self, direction, buttonIndex):
        # direction: "next" or "previous" depending on in which direction you want to go in the table pages.
        # buttonIndex: Index of the change page button. Starts at 0.
        # Clicks the button for the specified direction to change table pages

        tableParent = self.data["driver"].find_elements(By.XPATH, '//tfoot')[buttonIndex]
        if(direction.lower() == "next"):
            try:
                tableParent.find_element(By.XPATH, './/button[@title="Next page"][@id="pagination-next"][@tabindex="0"]').click()
                return True
            except:
                return False
        elif(direction.lower() == "previous"):
            try:
                tableParent.find_element(By.XPATH, './/button[@title="Previous page"][@id="pagination-back"][@tabindex="0"]').click()
                return True
            except:
                return False
        else:
            print("Wrong direction entered")
    
    def getInfoPanelDetails(self, infoName):
        # infoName: Name of the field inside an info panel
        # Returns: Value of the field corresponding to @infoName
        
        fieldName = self.data["driver"].find_element(By.XPATH, f'//div[@class="info-panel-title"][text()="{infoName}"]')
        fieldValue = fieldName.find_element(By.XPATH, '..//div[@class="info-panel-value"]').text
        
        return fieldValue

    def getNumberOfRowsInTable(self, index):
        # index: Table index is usually the number 'index' in div[3]/div[3]/div[2]/div[index] when you get XPATH for the table
        # Returns number of rows in the table of given index.
        numberOfRows = len(self.data["driver"].find_elements(By.XPATH, f'//*[@id="app"]/div/div[3]/div[3]/div[3]/div[2]/div[{index}]/div[3]/table/tbody/tr'))
        return numberOfRows

    def getNumberOfColumnsInTable(self, index):
        # index: Table index is usually the number 'index' in div[3]/div[3]/div[2]/div[index] when you get XPATH for the table
        # Returns number of columns in the table

        numberOfColumns = len(self.data["driver"].find_elements(By.XPATH, f'//*[@id="app"]/div/div[3]/div[3]/div[3]/div[2]/div[{index}]/div[3]/table/thead/tr/th'))
        return numberOfColumns

    def getTableHeaderName(self, column, index):
        # column: Index of the column you want the name of
        # index: Table index is usually the number 'index' in div[3]/div[3]/div[2]/div[index] when you get XPATH for the table
        # Returns the name of the column header

        try:
            columnHeader = self.data["driver"].find_element(By.XPATH, f'//*[@id="app"]/div/div[3]/div[3]/div[3]/div[2]/div[{index}]/div[3]/table/thead/tr/th[{column}]/span/div/div[1]')
            return columnHeader.text
        except:
            print("Column index out of range or not present")
            self.data["result"] = False

    def getElementFromTable(self, rowIndex, elementColumnIndex, isButton, index):
        # rowIndex Row number of the row you want the element from. Starts from 0
        # elementColumnIndex: Column number of the element inside the row. Can be found from the element's XPATH
        # isButton: True or False depending on if the element is a button. If True, then click the button
        # index: Table index is usually the number 'index' in div[3]/div[3]/div[2]/div[index] when you get XPATH for the table
        # Returns the web element from the given row and column. isButton: True if element is a button, false otherwise

        listOfRows = 0

        if (index == 1):
            listOfRows = self.data["driver"].find_elements(By.XPATH, f'//*[@id="app"]/div/div[3]/div[3]/div[3]/div[2]/div/div[3]/table/tbody/tr')
        else:
            listOfRows = self.data["driver"].find_elements(By.XPATH, f'//*[@id="app"]/div/div[3]/div[3]/div[3]/div[{index}]/div/div[3]/table/tbody/tr')

        # print(len(listOfRows))

        try:
            if isButton:
                element = listOfRows[rowIndex].find_element(By.XPATH, './/td[{}]/div[2]//div/button'.format(elementColumnIndex)).click()
                print("Clicked button at column {} for customer at {}".format(elementColumnIndex, rowIndex))
            else:
                element = listOfRows[rowIndex].find_element(By.XPATH, './/td[{}]/div[2]/div/div'.format(elementColumnIndex))
                return element
        except:
            print("There are no customers in the table")
            self.data["result"] = False

    def getHeaderInfo(self, columnNumber):
        # columnNumber: Column number on the screen. Usually starts from 1
        # Returns the data stored in the top column header on a page. Example: On overview page it will be the Margin and Bill Total at the very top

        try: #/html/body/div/div/div[3]/div[3]/div[3]/div[1]/ul/li[1]/span/span[2]/span
            columnName = self.data["driver"].find_element(By.XPATH, '//*[@id="app"]/div/div[3]/div[3]/div[3]/div[1]/ul/li[{}]/span/span[1]'.format(columnNumber)).text
            columnValue = self.data["driver"].find_element(By.XPATH, '//*[@id="app"]/div/div[3]/div[3]/div[3]/div[1]/ul/li[{}]/span/span[2]/span'.format(columnNumber)).text
            return [columnName, columnValue]
        except:
            print("Column index out of range or not present")
            self.data["result"] = False

    def sortByHeader(self, index, column, order):
        # index: Table index is usually the number 'index' in div[3]/div[3]/div[2]/div[index] when you get XPATH for the table
        # column: Index of the column you want to sort by
        # order: "asc" for ascending or "desc" for descending order
        # Sorts the table in the given order according to the given column

        try:
            if(order == "asc"):
                self.data["driver"].find_element(By.XPATH, f'//*[@id="app"]/div/div[3]/div[3]/div[2]/div[{index}]/div/div[3]/table/thead/tr/th[{column}]').click()
            elif(order == "desc"):
                self.data["driver"].find_element(By.XPATH, f'//*[@id="app"]/div/div[3]/div[3]/div[2]/div[{index}]/div/div[3]/table/thead/tr/th[{column}]').click()
                self.data["driver"].find_element(By.XPATH, f'//*[@id="app"]/div/div[3]/div[3]/div[2]/div[{index}]/div/div[3]/table/thead/tr/th[{column}]').click()

        except:
            print("Column index out of range or not present")
            self.data["result"] = False

    def convertAmountToFloat(self, amount):
        # amount: The amount string you are trying to convert to float
        # This method assumes that 'amount' is in format: {currency symbol} {amount}. Example: $100
        # Returns the converted amount as a float else 0

        try:
            return float(re.sub(',' , '', amount[1:].strip()))
        except:
            return 0

    def navigateToMenu(self, index, href):
        # index: Index of the menu button on the left navigation bar. Starts from 1
        # href: The href of the option inside a menu. Can be found from the XPATH of the option.
        # Navigates to the given menu from the left navigation bar
        # Example: navigateToMenu(7, '#/audittrail/resourcelifecycle') will open the resource life cycle page under audit trail

        time.sleep(2)
        try:
            self.data["driver"].find_element(By.XPATH, f'//*[@id="app"]/div/div[3]/div[1]/div[2]/div[{index}]').click()
            actions = ActionChains(self.data["driver"])
            actions.move_to_element(self.data["driver"].find_element(By.XPATH, f'//*[@id="app"]/div/div[3]/div[1]/div[2]/div[{index}]')).perform()
            menu = self.data["driver"].find_element(By.XPATH, f'//a[@href="{href}"]')
            self.data["driver"].execute_script("arguments[0].click();", menu)
            self.data["driver"].refresh()
        except:
            print("Invalid href or index given")
            
    def navigateGraphMenu(self, index, optionName, subOptionName, isDownload = True):
        # index: Index of the graph. Starts from 0
        # optionName: A string for which option to click. Usually Image, Data or Print
        # subOptionName: Index of the option inside the @optionName's menu. Starts from 1
        # isDownload: True if downloading file, False if just hovering

        time.sleep(2)
        try:
            self.data["driver"].find_elements(By.XPATH, f'//ul[@role="menubar"]')[index].click()
            actions = ActionChains(self.data["driver"])
            actions.move_to_element(self.data["driver"].find_element(By.XPATH, f'//li[@role="menuitem"]/a[text()="{optionName}"]')).perform()
            
            if (isDownload):
                actions.move_to_element(self.data["driver"].find_element(By.XPATH, f'//a[text()="{subOptionName}"]')).click().perform()
    
        except:
            print("Invalid index, optionName or subOptionName")

    def checkCustomers(self):
        # Returns True if customers are present in a table, else False

        try:
            self.data["driver"].find_element(By.XPATH, '//div[text()="No customers available"]')
            print("No customers available")
            return False
        except:
            return True

    def convertToDate(self, dateString, seperator, twoDates = False):
        # dateString: The date as a string which you want to convert to a date object
        # separator: The character by which the date is separated. Example '/' inside 10/4/2020
        # twoDates: True or False depending on if there are two dates inside the same dateString.
        # All date values inside tables are two dates in one string of format MM-DD-YYYY-MM-DD-YYYY
        # Returns the dateString as a date object

        if(not twoDates):
            mm, dd, yyyy = [int(x) for x in dateString.split(seperator)]
            if(mm > 12):
                date = datetime.date(yyyy, dd, mm)
            else:
                date = datetime.date(yyyy, mm, dd)
            return date

        else:
            j =  dateString.index(seperator, dateString.index(seperator) + 5)
            yCBS, mCBS, dCBS = [int(x) for x in dateString[:j].split(seperator)]
            yCBE, mCBE, dCBE = [int(x) for x in dateString[j + 1:].split(seperator)]
            return [datetime.date(yCBS, mCBS, dCBS), datetime.date(yCBE, mCBE, dCBE)]

    def selectRowsPerPage(self, optionIndex, menuIndex):
        # optionIndex: index of the option inside the rows per page drop down. Starts from 0
        # menuIndex: Rows Per Page menu number on the screen. Example: if there are 2 such menus then
        # the first one on screen will be index 0. Starts from 0
        # Changes rows per page for a table to the given option.

        try:
            rowsButton = self.data["driver"].find_elements(By.XPATH, '//*[@id="pagination-rows"]')[menuIndex]
            rowsButton.click()
            rowsMenu = self.data["driver"].find_elements(By.XPATH, '//*[@id="pagination-menu-list"]/li')
            rowsMenu[optionIndex].click()
        except:
            print("Wrong menu index or option index given")
