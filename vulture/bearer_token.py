import requests
import json

def get_bearer_token(user="demo@aquilaclouds.com", password="Aquila#Adm1n"):
    login_url = "https://qa-finops.aquilaclouds.com" + "/api/auth/login"
    login_payload = "{\"email\":\"" + user + "\",\"password\":\""+password+"\"}"
    login_headers = {
        'Content-Type': 'application/json',
    }
    s = requests.Session()

    login_response = requests.request("POST", login_url, headers=login_headers, data = login_payload)
    if login_response == None or login_response.status_code != 200:
        print("No response from server")
        return -1
    else:
        x = json.loads(login_response.text)
        return x['authToken']

