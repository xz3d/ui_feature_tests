import json
import sys
sys.path.append("/mnt/c/t/aquila/celebrimbor/scenario/test_home")
from vulture.bearer_token import *

from datetime import datetime
import difflib
#from jsondiff import diff

data_depth = 1

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def get_headers(user, password):
    bearer = get_bearer_token(user, password) 
    headers = {
        'Authorization': 'Bearer ' + str(bearer),
        'Content-Type': 'application/json'
    }
    return headers

def get_input_api(apiname):
    with open("../input/"+apiname+".json", 'r') as f:
        string = f.read()
        jsonstring = json.loads(string)
        return jsonstring["name"]

def get_input_data(apiname):
    with open("../input/"+apiname+".json", 'r') as f:
        string = f.read()
        jsonstring = json.loads(string)
        return jsonstring["data"]

def collect_to_output(apiname, response_str):
    with open("../output/"+apiname+".json", 'w') as f:
        f.write(response_str)

"""
def find_diff(a, b):
    result = []
    a = json.loads(a)
    b = json.loads(b)
    for key in a:
        if key not in b:
            result.append(f'{dict({key: a[key]})} -> {dict("key deleted"}')
        elif key in b and a[key] != b[key]:
            result.append(f'{dict({key: a[key]})} -> {dict({key: b[key]})}')
    return '\n'.join(t for t in result)
"""

def assert_output(apiname, response_str, output_str):
    if output_str == response_str:
        return (True, "")
    else:
        diff_string = diff(json.loads(output_str), json.loads(response_str))
        return (False, diff_string)

    '''
    if apiname == 'api_summary_fetch':
        data_depth = 64
    for i in range(0, data_depth):
        f = open("../output/"+apiname+str(i)+".json", 'r')
        string = f.read()
        f.close()
        if string == response_str:
            print('#################+####################')
            print(response_str)
            print('--------------------------------------')
            print(string)
            print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
            return (True, "")
        else:
            diff_string = diff(json.loads(string), json.loads(response_str))
            print('#################-####################')
            print(response_str)
            print('--------------------------------------')
            print(string)
            print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
            return (False, diff_string)
    '''
def aq_log_write(tcname, date, deployment_name, apiname, apidata, eval_res, output_name):
    with open("../output/logs/"+str(date).split()[0]+".log", 'a') as log:
        log.write(tcname + " " + str(date) + " deployment: " + deployment_name + " apiname: " + apiname + " input_data: " + apidata + " result: " + eval_res + " g_output_at: " + output_name + "\n")

def aq_collect(apiname):
    for i, j in enumerate(get_input_data(apiname)):
        response = requests.request("POST", get_input_api(apiname), headers=get_headers(), data=j)
        collect_to_output(apiname + str(i), response.text)
    return i

def aq_flag_write(filename, apiname, errname, flagname):
    with open(filename, 'a') as f:
        f.write(str(datetime.now()) + "|" + apiname + "|" + errname + "|" + flagname + "\n")

def aq_assert(apiname, tcname, sno):
    apicounter = 0
    serial = sno
    index = serial
    for i, j in enumerate(get_input_data(apiname)):
        response = requests.request("POST", get_input_api(apiname), headers=get_headers(), data=j)
        f = open("../output/"+apiname+str(i)+".json", 'r')
        output_str = f.read()
        f.close()
        result, err = assert_output(apiname, response.text, output_str)
        e = open("../output/diffs"+str(index)+".log", 'w')
        e.write(str(err))
        e.close()
        date_time = datetime.now()
        if result:
            print(bcolors.BOLD + "--------------------------------------------------" * 2 + bcolors.ENDC)
            print("{:3s} | {:30s} | {:40s} | {:3s} |".format(str(index), str(date_time), apiname, str(i)) + bcolors.OKGREEN + " OK" + bcolors.ENDC)
            #print(str(date_time) + '\t| ' + apiname + '\t\t\t\t| ' + bcolors.OKGREEN + " OK" + bcolors.ENDC)
            aq_log_write(tcname, str(date_time), "1", get_input_api(apiname), j, "PASS", "../output/"+apiname+str(i)+".json") 
        else:
            print(bcolors.BOLD + "--------------------------------------------------" * 2 + bcolors.ENDC)
            print("{:3s} | {:30s} | {:40s} | {:3s} |".format(str(index), str(date_time), apiname, str(i)) + bcolors.FAIL + " FAIL" + bcolors.ENDC + "\t|" + str(err))
            #print(str(date_time) + '\t| ' + apiname + '\t\t\t\t| ' + bcolors.FAIL + " FAIL" + bcolors.ENDC + " " + str(err))
            aq_log_write(tcname, str(date_time), "1", get_input_api(apiname), j, "FAIL", "../output/"+apiname+str(i)+".json")
            aq_flag_write("flags.flag", apiname, str(err),"RED")
        serial+=i
        index += 1 
    return result


def parse_xpath_settings():
    with open('xpaths.conf') as x:
        sstring = x.read()
        d = json.loads(sstring)
        return d